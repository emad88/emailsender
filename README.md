# README #

To test the application please visit the link below:

* [EmailSenderCSV](https://emailsendercsv.herokuapp.com)

### CSV file Format ###

* "email@address.com" ; first_name ; last_name


### Application ###
The application is written in: 

* Java 
* Spring boot 
* Maven 
* Intellij

* To parse the CSV file, Apache Commons is used


### Future Plans During Free Time ###
* Add more tests
* Add real functionality of sending email instead of mocking it with subject and message
* Handle errors more extensively in case there are any
* Make sure UI only accepts .csv file