app.controller("controller", function ($scope, $http, $window, $log) {

    $scope.upload = function () {
        $scope.header = "File is uploading, please be patient for results...";
        var file = $scope.myFile;
        var fd = new FormData();
        fd.append("file", file);
        $http.post('upload', fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
            .success(function (response) {
                $log.debug(response.code);
                var responseCode = response.code;

                if (responseCode != 200) {
                    if (responseCode == 400) {
                        $scope.header = "File is empty, please insert a CSV file with correct data format";
                    } else if (responseCode == 500) {
                        $scope.header = "Error! something went wrong, File couldn't be saved";
                    }
                } else if (responseCode == 200) {

                    showResultsPage();
                }
            })
            .error(function () {
                $scope.header = "Unknown error, please try again later.";
            });


    };

    function showResultsPage() {
        $window.location.href = '/result.html';
    }

    $scope.about = function () {
        $window.location.href = '/about.html';
    };
});

app.controller("resultController", function ($scope, $http, $window, $log) {

    var page = 1;
    var pageStatus;
    post(page);
    enableDisableButtons(page);


    function post(page) {
        $http.get('results', {params: {page: page}}).then(function mySucces(response) {
            $log.debug("Data: " + response.data.recipientList);
            $log.debug("lastpage: "+ response.data.lastPage);
            $scope.recipients = response.data.recipientList;
            pageStatus = response.data.lastPage;
            enableDisableButtons(page);

        }, function myError(response) {

        });
    }


    $scope.previous = function () {
        page--;
        post(page);
    };

    $scope.next = function () {
        page++;
        post(page);
    };

    function enableDisableButtons(page) {
        if (page == 1) {
            $scope.previousButton = true;
        } else {
            $scope.previousButton = false;
        }

        if (pageStatus){
            $scope.nextButton = true;
        } else {
            $scope.nextButton = false;
        }

    }


    $scope.home = function () {
        $window.location.href = '/index.html';
    }
});


app.controller("aboutController", function ($scope, $window) {
    $scope.home = function () {
        $window.location.href = '/index.html';
    };

});