package com.emailSender;

import configuration.SpringConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * Created by emadnikkhouy on 30/04/2017.
 */

public class EmailSenderApplication {

    public static void main(String[] args) {
        SpringApplicationBuilder builder = new SpringApplicationBuilder(SpringConfiguration.class);
        builder.headless(false);
        ConfigurableApplicationContext applicationContext = builder.run(args);
    }
}
