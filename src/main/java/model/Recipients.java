package model;

import java.util.List;

/**
 * Created by emadnikkhouy on 04/05/2017.
 */
public class Recipients {

    private List<Recipient> recipientList;
    private boolean lastPage;


    public List<Recipient> getRecipientList() {
        return recipientList;
    }

    public void setRecipientList(List<Recipient> recipientList) {
        this.recipientList = recipientList;
    }

    public boolean isLastPage() {
        return lastPage;
    }

    public void setLastPage(boolean lastPage) {
        this.lastPage = lastPage;
    }
}
