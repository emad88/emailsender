package controller;

import model.FileUpload;
import model.Recipients;
import org.apache.commons.csv.CSVParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import service.EmailSender;
import service.FileHandlerService;

import java.util.concurrent.Callable;

/**
 * Created by emadnikkhouy on 30/04/2017.
 */
@RestController
public class EmailSenderController {

    @Autowired
    private EmailSender emailSenderServiceMock;
    @Autowired
    private FileHandlerService fileHandlerService;
    private CSVParser parser;
    private boolean firstTime;

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public Callable<FileUpload> uploadFile(@RequestParam("file") MultipartFile file) throws Exception {
        firstTime = true;
        emailSenderServiceMock.reInitializeList();

        Callable<FileUpload> asyncResult = new Callable<FileUpload>() {
            @Override
            public FileUpload call() throws Exception {
                parser = fileHandlerService.getParser(file);
                FileUpload fileUpload = fileHandlerService.getUploadStatus();
                return fileUpload;
            }
        };

        return asyncResult;
    }

    @RequestMapping(value = "/results", method = RequestMethod.GET)
    public Callable<Recipients> processResults(@RequestParam(value = "page") int page ) {
        if (firstTime) {
            emailSenderServiceMock.sendEmails(parser, fileHandlerService.getFilePath());
            firstTime = false;
        }

        Callable<Recipients> asyncResult = new Callable<Recipients>() {
            @Override
            public Recipients call() throws Exception {
                return emailSenderServiceMock.getResults(page);
            }
        };
        return asyncResult;
    }

}
