package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by emadnikkhouy on 30/04/2017.
 */
@Controller
public class RedirectController {

    @RequestMapping("/")
    private String index(){
        return "redirect:/index.html";
    }
}
