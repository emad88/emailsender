package service;

import model.Recipient;
import model.Recipients;
import org.apache.commons.csv.CSVParser;

import java.util.List;

/**
 * Created by emadnikkhouy on 01/05/2017.
 */
public interface EmailSender {

    void sendEmails (CSVParser parser, String filePath);
    Recipients getResults(int page);
    void reInitializeList();
}
