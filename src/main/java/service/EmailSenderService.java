package service;

import model.Recipient;
import model.Recipients;
import org.apache.commons.csv.CSVParser;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by emadnikkhouy on 01/05/2017.
 */

// THIS CLASS IS FOR SENDING EMAILS IN REAL
// TO BE DONE IN FUTURE
@Component
public class EmailSenderService implements EmailSender {

    @Override
    public void sendEmails(CSVParser parser, String filePath) {

    }

    @Override
    public Recipients getResults(int page) {
        return null;
    }

    @Override
    public void reInitializeList() {

    }
}
