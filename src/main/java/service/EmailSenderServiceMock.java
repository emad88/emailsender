package service;

import model.Recipient;
import model.Recipients;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by emadnikkhouy on 01/05/2017.
 */
@Component
public class EmailSenderServiceMock implements EmailSender {

    private final int RETURN_SIZE = 5;
    private List<Recipient> recipients = new ArrayList<>();
    private String filePath;
    private int lastUsedIndex;
    private int lastUsedPage;
    private int lastReturnedListSize;

    @Override
    public void sendEmails(CSVParser parser, String filePath) {
        this.filePath = filePath;
        saveRecipientsList(parser);

        EmailValidator validator = EmailValidator.getInstance();

        for (Recipient recipient : recipients) {
            if (validator.isValid(recipient.getEmail())) {
                recipient.setStatus("Email has been sent");
            } else {
                recipient.setStatus("Wrong email address");
            }
        }

    }

    @Override
    public Recipients getResults(int page) {
        List<Recipient> tempList = new ArrayList<>();
        Recipients recipients = new Recipients();
        int listSize = this.recipients.size();
        int temp = 0;
        int index;

        index = getProperIndex(page);

        // set the end of page flag
        if (index + RETURN_SIZE >= listSize) {
            recipients.setLastPage(true);
        } else {
            recipients.setLastPage(false);
        }

        while (index < listSize) {
            tempList.add(this.recipients.get(index));
            index++;
            temp++;
            if (temp == RETURN_SIZE) {
                break;
            }
            lastUsedIndex = index;
        }
        lastUsedPage = page;
        lastReturnedListSize = tempList.size();
        recipients.setRecipientList(tempList);

        return recipients;

    }

    private int getProperIndex(int page) {
        int index;
        if (page == 1) {
            index = 0;
        } else {
            index = lastUsedIndex + 1;
        }
        // going back to the previous page
        if (lastUsedPage > page && page != 1) {
            index = lastUsedIndex - lastReturnedListSize - RETURN_SIZE;
        }
        return index;
    }

    @Override
    public void reInitializeList() {
        recipients.clear();
    }

    private void saveRecipientsList(CSVParser parser) {

        Recipient recipient = new Recipient();

        for (CSVRecord record : parser) {
            recipient.setEmail(record.get(0));
            recipient.setFirstName(record.get(1));
            recipient.setLastName(record.get(2));
            recipients.add(recipient);
            recipient = new Recipient();
        }
        deleteCSVFile();
    }

    private void deleteCSVFile() {
        Path path = Paths.get(filePath);
        try {
            Files.delete(path);
        } catch (NoSuchFileException x) {
            System.err.format("%s: no such" + " file or directory%n", path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
