package service;

import model.FileUpload;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by emadnikkhouy on 30/04/2017.
 */
@Component
public class FileHandlerService {

    private String filePath;
    private FileUpload fileUpload = new FileUpload();


    public CSVParser getParser(MultipartFile file) throws Exception {
        CSVParser parser = null;
        if (file.isEmpty()) {
            writeStatus(400);

        } else {
            File mFile = prepareFile(file);
            parser = prepareParser(mFile);
        }

        return parser;
    }

    public String getFilePath() {
        return filePath;
    }

    public FileUpload getUploadStatus() {
        return fileUpload;
    }

    private File prepareFile(MultipartFile file) {
        final String uploadingDir = System.getProperty("user.dir") + "/uploadingDir/";
        File f = new File(uploadingDir);

        if (!f.exists()) {
            f.mkdirs();
        }

        File mFile = new File(uploadingDir + System.currentTimeMillis() + file.getOriginalFilename());

        try {
            file.transferTo(mFile);
            writeStatus(200);
        } catch (IOException e) {
            writeStatus(500);
            e.printStackTrace();
        }
        filePath = mFile.getPath();

        return mFile;
    }

    private CSVParser prepareParser(File file) throws IOException {
        CSVFormat format = CSVFormat.newFormat(',')
                .withDelimiter(';')
                .withQuote('"')
                .withIgnoreEmptyLines(true)
                .withNullString("")
                .withTrim()
                .withIgnoreHeaderCase();

        CSVParser parser = new CSVParser(new FileReader(file), format);

        return parser;
    }

    private void writeStatus(int code) {

        switch (code) {
            case 400:
                fileUpload.setCode(400);
                break;
            case 500:
                fileUpload.setCode(500);
                break;
            case 200:
                fileUpload.setCode(200);
                break;

        }

    }

}
