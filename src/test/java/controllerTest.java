import configuration.SpringConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * Created by emadnikkhouy on 03/05/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = SpringConfiguration.class)
public class controllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Test
    public void testUpload() throws Exception {

       MockMultipartFile firstFile = new MockMultipartFile("file", "filename.csv", "undefined", "\"emadi@gmai\" ; emad ; nikkhouy ".getBytes());
        webAppContextSetup(this.webApplicationContext).build()
                .perform(fileUpload("/upload").file(firstFile))
                .andExpect(status().is(200));
    }


}
